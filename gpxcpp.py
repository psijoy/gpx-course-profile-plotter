#!/usr/bin/env python3
# # -*- coding: utf-8 -*-

"""GPX Course Profile Plotter -- main
    Python Version  : 3.5.5 in windows
    Created date    : 2017-03-27
"""
import gpxplot
import gpxpy.parser as parser
from geopy.distance import vincenty
from geopy.distance import great_circle
import ntpath

__author__  = 'Seijung Park'
__license__ = 'GPLv2'
__version__ = '0.2.11'
__date__    = '2018-04-05'



def get_nearest( points, wpt):
    near = []
    for i in range(len(points)):
        dist = vincenty( (points[i].latitude, points[i].longitude), (wpt.latitude, wpt.longitude)).meters
        if (dist < 20):
            print( "Found pos:%4d, dist:%3dm" %  (i, dist))
            near.append( points[i] )
            print( points[i] )
            return i
        #print( p, dist)
    return 0


class my_wpt:
    def __init__(self):
        self.name = []
        self.pos = 0    # x, y array position
        self.km = 0
        self.dkm = 0
        self.ascen = 0
        self.dascen = 0

    def __str__(self):
        return '[my_wpt{%s}:%s,%s %s: %s,%s]' % (self.name, self.pos, self.km, self.dkm, self.ascen, self.dascen)

    def get_nearest(self, points, wpt):
        self.name = wpt.name
        near = []
        for i in range(len(points)):
            dist = vincenty( (points[i].latitude, points[i].longitude), (wpt.latitude, wpt.longitude)).meters
            if (dist < 20):
                print( "Found pos:%4d, dist:%3dm" %  (i, dist))
                self.name = wpt.name
                self.pos = i
                print( points[i] )
                return i
        return 0

    def set_last(self, points):
        self.name = '완주'
        self.pos = len(points) - 1
        return
            
    def get_km( self, prev, x):
        self.km = x[self.pos]
        if prev is None:
            self.dkm = self.km
        else:
            self.dkm = self.km - prev.km
        return

    def get_ascen( self,  prev, y):
        self.ascen = y[self.pos]
        if prev is None:
            self.dascen = self.ascen
        else:
            self.dascen = self.ascen - prev.ascen
        return
    


def calc_dist( points):
    x = []
    y = []
    ascen = []

    lenth = 0
    upelev = 0
    print( "\n" )
    lenth = len( points )
    point0 = points[0]
    point1 = points[1]

    sum1 = 0
    sum2 = 0
    x.append(0)
    y.append(0)
    ascen.append(0)
    
    for i in range(1, lenth):
        lat0 = points[i-1].latitude
        lon0 = points[i-1].longitude
        lat1 = points[i].latitude
        lon1 = points[i].longitude
    
        dist1 = vincenty( (lat0, lon0), (lat1, lon1)).meters
        sum1 += dist1
        if dist1 == 0:
            dist1 = 0.5     # defend zero devide
        sum2 = sum1 / 1000

        elev = points[i].elevation
        delev = (elev - points[i-1].elevation)

        # factor(4.0) is calibrated. do not modify
        # ignore single up point
        #el0 = 
        #el1
        #el2
        if delev > 4.0:
            upelev += delev
        grad = delev / dist1 * 100
        #print( "%7.1f, %8.3f %5.1f %4.1f" % (dist1, sum2, elev, grad))
        x.append(sum1/1000)
        y.append(elev)
        ascen.append(upelev)
    print( "Total up elevation: %f\n" % (upelev))
    return x, y, ascen


    
def do_job(f_name, colorize=None):
    gpx_file = open( f_name, 'r', encoding='UTF8')
    wpt_list = []
    s_list = []

    gpx_parser = parser.GPXParser( gpx_file )
    gpx_parser.parse()

    gpx_file.close()

    gpx = gpx_parser.parse()
    x, y, ascen = calc_dist(gpx.tracks[0].segments[0].points)

    for waypoint in gpx.waypoints:
        #print ('waypoint {0} -> ({1},{2})'.format( waypoint.name, waypoint.latitude, waypoint.longitude ))
        print( waypoint )
        #pos = get_nearest( gpx.tracks[0].segments[0].points, waypoint)
        w = my_wpt()
        pos = w.get_nearest( gpx.tracks[0].segments[0].points, waypoint)
        
        if w.name[0] == 's':
            s_list.append(w)
        else:
            wpt_list.append(w)

    wpt_list.sort(key=lambda my_wpt : my_wpt.pos)
        
     
    for i in range(len(wpt_list)):
        if i == 0:
            wpt_list[i].get_km( None, x)
            wpt_list[i].get_ascen( None, ascen)
        else:
            wpt_list[i].get_km( wpt_list[i-1], x)
            wpt_list[i].get_ascen( wpt_list[i-1], ascen)

            #wpt.append(pos)
            #wpt_name.append(waypoint.name)

    if (len(x) - wpt_list[-1].pos) > 5:
        w = my_wpt()
        w.set_last( gpx.tracks[0].segments[0].points )
        
        if len(wpt_list) == 0:
            w.get_km( None, x)
            w.get_ascen( None, ascen)
        else:
            print( len(x), len(ascen))
            w.get_km( wpt_list[-1], x)
            w.get_ascen( wpt_list[-1], ascen)
        wpt_list.append( w )
    
    for w in wpt_list:
        print(w)

    name = ntpath.basename(f_name)
    gpxplot.do_plot(x,y, wpt_list, s_list, name, colorize)
   
    return


def main():
    #do_job('D:/psj/개인자료/자전거/경로/50란도너스/2018/1서울/S-200K.gpx', colorize='y')
    #do_job('D:/psj/개인자료/자전거/경로/50란도너스/2018/1서울/S-300K-edited.gpx', colorize='y')
    #do_job('D:/psj/개인자료/자전거/경로/50란도너스/2018/1서울/S-400K-edited.gpx', colorize='y')
    #do_job('D:/psj/개인자료/자전거/경로/50란도너스/2018/5부산/부산400/B-400K-2-edited.gpx', colorize='y')
    #do_job('D:/psj/개인자료/자전거/경로/50란도너스/2018/5부산/부산600/B-600K-1.gpx', colorize='y')
    #do_job('D:/psj/개인자료/자전거/경로/50란도너스/2018/5부산/부산600/B-600K-2.gpx', colorize='y')
    do_job('D:/psj/개인자료/자전거/경로/50란도너스/2018/9플레쉬/44 비단길/플레쉬-비단길.gpx', colorize='y')
    #do_job('D:/psj/개인자료/자전거/경로/50란도너스/2018/3천안/천안400/C-400K-edited.gpx', colorize='y')
    #do_job( '../GPX/sample0.gpx')
    #do_job('D:/psj/개인자료/자전거/경로/50란도너스/2018/9플레쉬/2018 flash extra-01.gpx', colorize='y')
    #do_job('D:/psj/개인자료/자전거/경로/50란도너스/2018/9플레쉬/2018 flash pre test.gpx', colorize='y')
	
	
if __name__ == '__main__':
	main()
