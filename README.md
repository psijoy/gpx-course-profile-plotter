GPX Course Profile Poltter
 
  It is originally made for Korea Randonneurs Course(http://www.korearandonneurs.kr/).
  Anyone can use this for his own GPX cours profile visualization under GPLv2 license.
  
  Created course profile samples is here below.
  https://cdn.clien.net/web/api/file/F01/6813355/5736a7ea13f49e.png?thumb=true
  
  Usage
  
  0. gpxcpp.py is main program. gpxplot is plotting module.
     When you run gpxcpp.py, the png file is created same directory that program is.
     
  1. Need GPX file of course, which contains elevation profile and waypoints.
  
  2. Elevation should aberve see see level, or the program does no plot that point.
  
  3. Waypoint has two kind. CP(Control Poind) and Summit.
     CP - Should start number eg: 1.Seoul, 2.NY...
           optional suffix '_xy' for position
                      where x : 'R' - CP name and dist / elev infomation is printed right
                                'L' - print left
                               (default, left)
                            y : vertical position, 9 uppermost, 1, lowermost
                               (no default value, you have to apply value explictly)
                      if first CP is too close from start, also printed right
     Summit - Prefix 's' eg: sChang La, sNamSan
              optional Postfix '_xy' for name position
                      where x : u-upper, U-upper*2, c,C-center, l-lower, L-lower*2
                            y : 1-left, 2,5-center, 3-right, 4-left*2, 6-right*2
                            default: '_U3'
     Summit(Marker) is printed yellow triangle with its name
	 
  4. Waypoint must placed within 20m radious from any course point, not course line.
     If some waypoint is missing in plot, verify the position of that waypoint.
     
  5. Now, you can colorize the elevation profile for more information.
     Call do_job() with colorize='y' option.
     If you don't want colorized profile, just call with gpx file path.
     ex: do_job( 'some_gpx_file.gpx', colorize='y')
         do_job( 'gpx_file_does_not_not_want_to_colorize.gpx' ) 
	 
  6. This program is not user friendly, You have to modify the source(pgxcpp.py)
     to handle another GPX file. At least, change GPX file name.
     
  7. Some font file path should changed, if you have not installed proper font(s).
  
  8. If you have any problems to install / run this program, don't ask me.
     Google it.
	 
  9. If you have any questions about Randonneurs, refer links below.
     http://www.korearandonneurs.kr/
	 http://www.audax-club-parisien.com/EN/index.php
	 https://rusa.org/
	 https://www.audax-japan.org/en/audax-japan/
	 http://app.audaxthailand.com/home
	 
	 Enjoy Riding !!!
  
  